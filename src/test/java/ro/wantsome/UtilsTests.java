package ro.wantsome;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class UtilsTests {
    @Test
    public void testPalindromeFinderIdentifiesPalindromes(){
        Set<String> inputSet = new HashSet<>();
        inputSet.add("asa");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        assertTrue(palindromes.contains("asa"));
    }
    @Test
    public void testPalindromeFinder_findsMultipes(){
        Set<String> inputSet = new HashSet<>();
        inputSet.add("asa");
        inputSet.add("ata");
        inputSet.add("cojoc");

        Set<String> palindromes = Utils.findPalindromes(inputSet);
        assertEquals(3, palindromes.size());
    }
}
