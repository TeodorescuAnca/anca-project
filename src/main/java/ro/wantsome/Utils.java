package ro.wantsome;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Utils {
    /**
     * Can read a given file from the resources folder
     *
     * @param fileName file name
     * @return list of all the lines in the file
     */
    public static List<String> readAllLinesFromResourcesFile(String fileName) throws IOException {
        List<String> result = new ArrayList<>();
        ClassLoader classLoader = Utils.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        }
        return result;
    }

    /**
     * Can read a given file in the working directory
     *
     * @param fileName file name
     * @return list of all the lines in the file
     */
    public static List<String> readAllLinesFromSourceFile(String fileName) throws IOException {
        return Files.readAllLines(Paths.get(fileName));
    }

    public static Set<String> removeDuplicates(List<String> allLines) {
        Set<String> wordsSet = new HashSet<>();
        for (String line : allLines) {
            wordsSet.add(line);
        }
        return wordsSet;
    }

    public static Set<String> findWordsThatContain(Set<String> wordsSet, String word) {
        Set<String> result = new HashSet<>();
        for (String line : wordsSet) {
            if (line.contains(word))
                System.out.println(line);
        }
        return result;
    }

    public static Set<String> findPalindromes(Set<String> wordsList) {
        Set<String> result = new HashSet<>();
        for (String line : wordsList) {

            StringBuilder reversedWord = new StringBuilder(line).reverse();

            // reverse StringBuilder input1
//            reversedWord = reversedWord.reverse();

            if (line.equals(reversedWord.toString())) {
                result.add(line);
            }

        }
        return result;
    }

}
