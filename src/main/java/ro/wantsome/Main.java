package ro.wantsome;

import ro.wantsome.Utils;

import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] arg) throws IOException {
        List<String> allLines = new ArrayList<>();

        allLines = Utils.readAllLinesFromResourcesFile("dex.txt");

        Set<String> wordsSet = Utils.removeDuplicates(allLines);

        // Using Scanner for Getting Input from User
        Scanner in = new Scanner(System.in);

        while (true) {

            String userGivenWord = in.nextLine();

            if ("".equals(userGivenWord))
                break; // or exit (0);

            Set<String> resultedWord = Utils.findWordsThatContain(wordsSet, userGivenWord);

            for (String line : resultedWord) {
                System.out.println(line);
            }
        }
    }
}
